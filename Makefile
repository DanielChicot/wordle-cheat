

virtualenv:
	pip install virtualenv

venv: virtualenv
	virtualenv venv

requirements: venv
	. ./venv/bin/activate && pip install -r requirements.txt

