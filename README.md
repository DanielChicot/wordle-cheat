# Wordle Cheat

## Cheat at wordle

### Usage

   ```
   usage: Wordle helper [-h] [-e EXCLUSIONS] [-w WORDS_FILE] attempts [attempts ...]

   Cheat at wordle

   positional arguments:
     attempts              Efforts made

   options:
     -h, --help            show this help message and exit
     -e EXCLUSIONS, --exclusions EXCLUSIONS
                           Letters not in the solution
     -w WORDS_FILE, --words-file WORDS_FILE
                           Words file location
   ```

### Example

Consider the efforts made in the image at the end of this document.

For each attempt made so far in wordle add an attempt to the arguments list. Replace any letters
that are not in the solution with a `.`. Replace any letters in the attempt that are in the solution
but not in the correct position (i.e. those letters marked as yellow) with a lower-case rendition 
of that letter. Replace any letters that are in the solution and are in the correct position (i.e.
those letter marked green) with an upper-case rendition of that letter. Any letters that are not 
in the solution should be added to the exclusions optional argument string.

  ```
  ./main.py -e udzb a.ie. .ai.E
  craie
  hiate
  image
  inane
  irate
  liane
  minae
  viage
  vitae
  ```
Search the words list for 
* (application of exclusions)
  * words that do not contain the letters `u`, `d`, `z` or `b`
* (application of first attempt `a.ie.` corresponding to `ADIEU` in the image)
  * that do contain an `a` but not in position 1
  * that do contain `i` but not in position 3
  * that do contain `e` but not in position 4
* (application of the second attempt `.ai.E` corresponding to `BAIZE` in the image)
  * that do contain an `a` but not in position 2
  * that do contain `i` but not in position 3
  * that do contain `e` in position 5

![Effort](wordle.jpg)


### Requirements

* Python 3.10+
* Make

### Set-up
  ```bash
  make requirements
  . ./venv/bin/activate
  ```