#!/usr/bin/env python

import argparse
import re
import sys
from functools import reduce
from typing import Callable

from returns.curry import curry
from returns.io import impure_safe, IOSuccess, IOFailure
from returns.pipeline import flow
from returns.pipeline import pipe
from returns.pointfree import map_
from returns.unsafe import unsafe_perform_io


def main():
    words_file, exclusions, attempts = arguments()
    match flow(all_words(words_file),
               map_(possibilities(exclusions, attempts))):
        case IOSuccess(value):
            [print(word) for word in unsafe_perform_io(value)]
        case IOFailure(value):
            sys.stderr.write(f"Failed: '{unsafe_perform_io(value)}'.")


@curry
def possibilities(exclusions: str, attempts: list[str]) -> Callable[[list[str]], list[str]]:
    return pipe(five_letter_words,
                misses_removed(exclusions),
                required_letters(attempts),
                direct_hits(attempts),
                wrong_position(attempts))


@curry
def wrong_position(attempts: str, words: list[str]) -> list[str]:
    return reduce(misplaced, attempts, words)


@curry
def required_letters(attempts: str, words: list[str]) -> list[str]:
    """

    @param attempts:
    @param words:
    @return:
    """
    return reduce(containing_letter, musts(attempts), words)


@curry
def containing_letter(words: list[str], must: str) -> list[str]:
    return filtered(lambda x, y: bool(re.compile(x).search(y)), words, must)


def musts(attempts: str) -> set[str]:
    concatenated = re.sub(r"[.A-Z]", "", reduce(lambda accumulation, attempt: accumulation + attempt, attempts))
    return set((x for x in concatenated))


@curry
def direct_hits(attempts: str, words: list[str]) -> list[str]:
    return reduce(lambda accumulation, attempt: matched(accumulation, re.sub("[a-z]", '.', attempt).lower()),
                  attempts, words)


@curry
def misses_removed(exclusions: str, words: list[str]) -> list[str]:
    """
    Filter a list of words to exclude those that contain any of the letters in the exclusions string.
    @param exclusions: the letters that none of the results must contain
    @param words: the list of words to filter
    @return: a list with the exclusions removed
    """
    return filtered(lambda x, y: not re.compile(x).search(y), words, "[" + exclusions + "]")


def misplaced(words: list[str], attempt: str) -> list[str]:
    return reduce(lambda accumulation, pair: unmatched(accumulation, misplaced_letter_pattern(pair[0], pair[1])),
                  misplaced_letters(attempt), words)


def misplaced_letter_pattern(index: int, letter: str) -> str:
    """
    >>> misplaced_letter_pattern(-1, 'a')
    '.....'
    >>> misplaced_letter_pattern(0, 'b')
    'b....'
    >>> misplaced_letter_pattern(1, 'c')
    '.c...'
    >>> misplaced_letter_pattern(2, 'd')
    '..d..'
    >>> misplaced_letter_pattern(3, 'e')
    '...e.'
    >>> misplaced_letter_pattern(4, 'f')
    '....f'
    >>> misplaced_letter_pattern(5, 'g')
    '.....'

    :param index: where to place the letter in the result
    :param letter: the letter to insert into the result
    :return: a 5 character long regex pattern consisting of '.' in every postion except at
             the position of the 'index' argument where there will be the 'letter' argument.
    """
    return '.....'[:index] + letter + '.....'[index + 1:] if -1 < index < 5 else '.....'


def misplaced_letters(attempt: str) -> list[tuple[int, str]]:
    misplaced_chars = re.sub("[A-Z]", '.', attempt).lower()
    return [(i, misplaced_chars[i]) for i in range(0, 5) if misplaced_chars[i].isalpha()]


def five_letter_words(words: list[str]) -> list[str]:
    """
    >>> five_letter_words(['Abcde', 'abcde', 'abcdef', 'abcd', 'fghij'])
    ['abcde', 'fghij']
    """
    return [x for x in matched(words, "[a-z]{5}")]


@impure_safe
def all_words(words_file: str) -> list[str]:
    with open(words_file) as f:
        return [x.strip() for x in f.readlines()]


def matched(words: list[str], pattern: str) -> list[str]:
    return filtered(lambda x, y: bool(re.compile(f"^{x}$").search(y)), words, pattern)


def unmatched(words: list[str], pattern: str) -> list[str]:
    return filtered(lambda x, y: not re.compile(f"^{x}$").search(y), words, pattern)


def filtered(include: Callable[[str, str], bool], words: list[str], pattern: str) -> list[str]:
    return [x for x in words if include(pattern, x)]


def arguments() -> tuple[str, str, list[str]]:
    args = argument_parser().parse_args()
    return args.words_file, args.exclusions, args.attempts


def argument_parser():
    parser = argparse.ArgumentParser(prog='Wordle helper', description='Cheat at wordle')
    parser.add_argument('-e', '--exclusions', help='Letters not in the solution')
    parser.add_argument('-w', '--words-file', help='Words file location', default='/usr/share/dict/words')
    parser.add_argument('attempts', help='Efforts made', nargs='+')
    return parser


if __name__ == '__main__':
    main()
